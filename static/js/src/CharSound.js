'use strict';
/* global EngSolo */



/**
 *
 * @param {number} num
 * @param {string} id
 * @constructor
 */
EngSolo.CharSound = function(num, id) {
  this.num_ = num;
  this.id_ = id;
};


/**
 *
 * @return {number}
 */
EngSolo.CharSound.prototype.getNum = function() {
  return this.num_;
};


/**
 *
 * @return {string}
 */
EngSolo.CharSound.prototype.getId = function() {
  return this.id_;
};
