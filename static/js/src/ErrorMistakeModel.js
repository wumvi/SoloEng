'use strict';
/* global EngSolo */



/**
 * @param {number} errorCount
 * @param {boolean} isToMatchError
 * @param {number} errorTimeout
 * @constructor
 */
EngSolo.ErrorMistakeModel = function(errorCount, isToMatchError, errorTimeout) {
  /**
   *
   * @type {number}
   * @private
   */
  this.errorCount_ = errorCount;

  /**
   *
   * @type {boolean}
   * @private
   */
  this.isToMatchError_ = isToMatchError;

  /**
   * @type {number}
   * @private
   */
  this.errorTimeout_ = errorTimeout;
};


/**
 *
 * @return {number}
 */
EngSolo.ErrorMistakeModel.prototype.getErrorCount = function() {
  return this.errorCount_;
};


/**
 *
 * @return {number}
 */
EngSolo.ErrorMistakeModel.prototype.getErrorTimeout = function() {
  return this.errorTimeout_;
};


/**
 *
 * @return {boolean}
 */
EngSolo.ErrorMistakeModel.prototype.isToMatchError = function() {
  return this.isToMatchError_;
};
