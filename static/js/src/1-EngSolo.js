'use strict';
/* global EventDomDispatcher */



/**
 * @param {EngSolo.MapBase} map
 * @param {!jQuery} $root
 * @param {string} subCategory
 * @constructor
 * @export
 */
function EngSolo(map, $root, subCategory) {
    /**
     * Модель карты
     * @type {EngSolo.MapBase}
     * @private
     */
    this.map_ = map;

    /**
     * Root объект
     * @type {!jQuery}
     * @private
     */
    this.$root_ = $root;

    /**
     * Номер линии которую печатаем
     * @type {number}
     * @private
     */
    this.lineNum_ = 0;

    /**
     * Номер символа в линии, которые печатаем
     * @type {number}
     * @private
     */
    this.charNum_ = 0;

    /**
     * Глобальный, сквозной номер символа который печатаем
     * @type {number}
     * @private
     */
    this.globalCharNum_ = 0;

    /**
     * Объект линии
     * @type {jQuery}
     * @private
     */
    this.$lineList_ = null;

    /**
     * Список элементов линии
     * @type {jQuery}
     * @private
     */
    this.$charList_ = null;

    /**
     * Количество разрешенных ошибок
     * @type {number}
     * @private
     */
    this.errorCount_ = this.map_.getErrorCount();

    /**
     * Блокировать ли обработку нажатых клавишь
     * @type {boolean}
     * @private
     */
    this.canKeyPress_ = true;

    /**
     * Handle на обратный отсчёт в случае ошибки
     * @type {?number}
     * @private
     */
    this.errorIntervalHandle_ = null;

    /**
     *
     * @type {number}
     * @private
     */
    this.mistakeCurrentTime_ = 0;

    /**
     * Активное ли задание
     * @type {boolean}
     * @private
     */
    this.isActive_ = true;

    /**
     * Диспетчер событий
     * @type {EventDomDispatcher}
     * @private
     */
    // this.eventDomDispatcher_ = new EventDomDispatcher(EngSolo.EVENT_NAME, subCategory);

    /**
     * Таймаут на ошибку
     * @type {number}
     * @private
     */
    this.errorTimeout_ = EngSolo.TIMEDOWN_FOR_ERROR;

    this.init_();
}


/**
 *
 * @private
 */
EngSolo.prototype.init_ = function() {
    this.initMap_();

    this.$lineList_ = this.$root_.find(EngSolo.LINE_JS);
    this.clearAll_();

    this.initEvent_();
};


/**
 *
 * @private
 */
EngSolo.prototype.initMap_ = function() {
    var htmlText = '';

    var globalPos = 0;

    // Строим HTML по карте
    var list = this.map_.getLineList();
    for (var lineNum = 0; lineNum < list.length; lineNum += 1) {
        htmlText += '<div class="sgm-line sgm-line__js"><div class="sgm-inner">';

        // Бегаем по линиям
        for (var charNum = 0; charNum < list[lineNum].length; charNum += 1) {
            // Получаем символ, который нужно отображить
            var charHide = this.map_.getCharHide(globalPos);
            var char = charHide ? charHide : list[lineNum][charNum];
            var className = char === ' ' ? 'sgm-space-flag' : '';

            htmlText += '<span class="sgm-char sgm-char__js ' + className + '" data-num="' +
                globalPos + '">' + char + '</span>';
            globalPos += 1;
        }

        htmlText += '</div></div>';
    }

    this.$root_.html(htmlText);
};


/**
 *
 * @private
 */
EngSolo.prototype.initEvent_ = function() {
    jQuery(document).on('keydown', this.onKeyDownAction_.bind(this));
    this.$root_.click(this.onClick.bind(this));
};


/**
 *
 */
EngSolo.prototype.onClick = function() {
    if (!this.isActive_) {
        this.$root_.removeClass('sgm-not-active-flag');
        this.isActive_ = true;
    }
};


/**
 *
 * @param {number} timeout
 * @export
 */
EngSolo.prototype.setErrorTimeout = function(timeout) {
    this.errorTimeout_ = timeout;
};


/**
 *
 * @return {number}
 */
EngSolo.prototype.getErrorTimeout = function() {
    return this.errorTimeout_;
};


/**
 *
 * @param {HTMLElement} elem
 * @return {boolean}
 * @private
 */
EngSolo.prototype.isInputTyping_ = function(elem) {
    return (elem.tagName.toUpperCase() === 'INPUT' &&
            (
            elem.type.toUpperCase() === 'TEXT' ||
            elem.type.toUpperCase() === 'PASSWORD' ||
            elem.type.toUpperCase() === 'FILE' ||
            elem.type.toUpperCase() === 'SEARCH' ||
            elem.type.toUpperCase() === 'EMAIL' ||
            elem.type.toUpperCase() === 'NUMBER' ||
            elem.type.toUpperCase() === 'DATE')
        ) ||
        elem.tagName.toUpperCase() === 'TEXTAREA';
};


/**
 *
 * @param {jQuery.Event} event
 * @private
 */
EngSolo.prototype.onKeyDownAction_ = function(event) {
    var doPrevent = false;
    var elem = event.srcElement || event.target;

    // Блокировка backspace
    if (event.keyCode === 8) {
        if (this.isInputTyping_(elem)) {
            doPrevent = elem.readOnly || elem.disabled;
        } else {
            doPrevent = true;
        }
    } else {
        // Если выбран не документ, то мы набираем текст для задания
        // в ином случае, надо сделать не активным задание
        if (this.isInputTyping_(elem) && !elem.readOnly && !elem.disabled) {
            this.$root_.addClass('sgm-not-active-flag');
            this.isActive_ = false;
            return;
        }


        var charCode = -1;
        var codeKey = event.originalEvent.code;

        // Когда сидим под IE, надо сделать обработку ctrl, shift, alft
        var ieChar = event.originalEvent.char;
        if (ieChar !== undefined && ieChar.length !== 1) {
            return;
        }

        // Если мы сидим под IE, то codeKey не передаётся как обычному Chrome
        if (codeKey === undefined) {
            charCode = event.originalEvent.keyCode;
        } else if (/^Key[A-Z]$/.test(codeKey)) {
            charCode = codeKey.substr(3).charCodeAt(0);
        } else if (codeKey === 'Space') {
            charCode = 32;
            // event.preventDefault();
        } else if (codeKey === 'NumpadEnter' || codeKey === 'Enter') {
            return;
        } else if (/^F\d+$/.test(codeKey)) {
            return;
        }

        if (!this.canKeyPress_) {
            event.preventDefault();
            return;
        }

        if (charCode !== -1) {
            this.keyPress_(charCode);
            doPrevent = true;
        }
    }

    if (doPrevent) {
        event.preventDefault();
    }
};


/**
 *
 * @param {number} charCode
 * @private
 */
EngSolo.prototype.keyPress_ = function(charCode) {
    // Игнор на клавишу Enter
    if (charCode === 13) {
        return;
    }

    // Получаем список линий
    var list = this.map_.getLineList();

    // Если игра пройдена, игнорим нажания
    if (this.lineNum_ >= list.length) {
        return;
    }

    // Получаем текущий символ, который нужно напечатать
    var charInStr = list[this.lineNum_][this.charNum_].toLowerCase();//.charCodeAt();

    //this.eventDomDispatcher_.emit(EngSolo.EVENT_KEY_PRESS, new EngSolo.KeyPressEvent(this.globalCharNum_));

    // Проверяем, правильно ли мы нажали
    if (String.fromCharCode(charCode).toLowerCase() === charInStr) {
        this.makeRightKeyAction_();
    } else {
        this.makeWrongKeyAction_();
    }
};


/**
 *
 * @private
 */
EngSolo.prototype.makeWrongKeyAction_ = function() {
    // Уменьшем количество допустимых ошибок
    this.errorCount_ -= 1;

    // Если количество допустимых ошибок, слишком много, то очищаем всё
    if (this.errorCount_ === -1) {
        // this.$root_.addClass(EngSolo.TO_MATCH_MISTAKE_FLAG);

        this.clearAll_();

        /*this.eventDomDispatcher_.emit(
            EngSolo.EVENT_TOO_MATCH_ERROR,
            new EngSolo.ErrorMistakeModel(this.errorCount_, true, this.errorTimeout_)
        ); */
    } else {
        /*this.eventDomDispatcher_.emit(
            EngSolo.EVENT_MAKE_MISTAKE,
            new EngSolo.ErrorMistakeModel(this.errorCount_, false, this.errorTimeout_)
        );*/

        this.mistakeCurrentTime_ = this.errorTimeout_;
        this.errorIntervalHandle_ = setInterval(this.intervalCountDownAction_.bind(this), 1000);
        this.$root_.addClass(EngSolo.MISTAKE_FLAG);
        this.canKeyPress_ = false;
        jQuery(this.$charList_[this.charNum_]).addClass(EngSolo.WRONG_CHAR_FLAG);
    }
};


/**
 *
 * @private
 */
EngSolo.prototype.intervalCountDownAction_ = function() {
    this.mistakeCurrentTime_ -= 1;
    //this.eventDomDispatcher_.emit(EngSolo.EVENT_ERROR_TIMEOUT, this.mistakeCurrentTime_);
    if (this.mistakeCurrentTime_ === 0) {
        clearInterval(this.errorIntervalHandle_);
        this.errorIntervalHandle_ = null;
        this.$root_.removeClass(EngSolo.MISTAKE_FLAG);
        this.canKeyPress_ = true;
    }
};


/**
 * Перезапуск игры
 */
EngSolo.prototype.resetGame = function() {
    this.clearAll_();
};


/**
 * @private
 */
EngSolo.prototype.makeRightKeyAction_ = function() {
    // Добавляем класс, что нажали мы правильно
    jQuery(this.$charList_[this.charNum_]).removeClass([EngSolo.WRONG_CHAR_FLAG, EngSolo.CURRENT_CHAR_FLAG].join(' '))
        .addClass(EngSolo.RIGHT_CHAR_FLAG);

    this.charNum_ += 1;
    this.globalCharNum_ += 1;

    // Получаем модель слова, которое должно произнести, если оно есть
    var charSound = this.map_.getCharSound(this.globalCharNum_);
    if (charSound) {

        var nextCharSound = this.map_.getNextSoundChar_(this.globalCharNum_);
        if (nextCharSound) {
            this.map_.loadAudio(nextCharSound.getId());
        }

        // Воспроизводим слово
        this.playSound_(charSound);
    }

    // Получаем список линий
    var list = this.map_.getLineList();

    // Если это конец линии
    if (list[this.lineNum_].length === this.charNum_) {
        this.$lineList_.eq(this.lineNum_).addClass(EngSolo.LINE_DONE_FLAG);

        // Обнуляем позицию
        this.charNum_ = 0;
        // Переходим на след. линию
        this.lineNum_ += 1;

        // Если линии закончились, выходим. Пользователь удачно всё прошёл
        if (this.lineNum_ === list.length) {
            //this.eventDomDispatcher_.emit(EngSolo.EVENT_END_GAME, null);
            return;
        }

        this.$charList_ = this.$lineList_.eq(this.lineNum_).find(EngSolo.CHAR_JS);
    }

    // Помечаем следующий символ, текущим, который нужно напечатать
    jQuery(this.$charList_[this.charNum_]).addClass(EngSolo.CURRENT_CHAR_FLAG);

    //this.eventDomDispatcher_.emit(EngSolo.EVENT_KEY_PRESS_RIGHT, new EngSolo.KeyPressEvent(this.globalCharNum_));
};


/**
 *
 * @param {EngSolo.CharSound} charSound
 * @private
 */
EngSolo.prototype.playSound_ = function(charSound) {
    this.map_.playSound(charSound.getId());
};


/**
 *
 * @private
 */
EngSolo.prototype.clearAll_ = function() {
    // Выставляем текущую линию первой
    this.lineNum_ = 0;
    // Выставляем текущий символ первым
    this.charNum_ = 0;
    // Выставляем текущий глобальный символ первым
    this.globalCharNum_ = 0;

    // Очищаем количество ошибок
    this.errorCount_ = this.map_.getErrorCount();
    // Выставляем текущей первую линию
    this.$charList_ = this.$lineList_.eq(0).find(EngSolo.CHAR_JS);
    // Убераем флаги: правильный символ, текщий символ, неправильный символ
    this.$root_.find(EngSolo.CHAR_JS).removeClass(
        [EngSolo.RIGHT_CHAR_FLAG, EngSolo.CURRENT_CHAR_FLAG, EngSolo.WRONG_CHAR_FLAG].join(' ')
    );
    // Выставляем текущий символ в ноль
    this.$charList_.eq(0).addClass(EngSolo.CURRENT_CHAR_FLAG);
    // Флаг Линия сделана
    this.$lineList_.removeClass(EngSolo.LINE_DONE_FLAG);
};


/**
 * Устанавливаем, что данный объект активный и может перехватывать нажатия клавиш
 * @param {boolean} flag
 */
EngSolo.prototype.setActive = function(flag) {
    this.canKeyPress_ = flag;
};


/**
 * Линия пройдена
 * @const {string}
 */
EngSolo.LINE_DONE_FLAG = 'sgm-line-done-flag';


/**
 * Текущий символ
 * @const {string}
 */
EngSolo.CURRENT_CHAR_FLAG = 'sgm-current-char-flag';


/**
 * Напечатан правильный символ
 * @const {string}
 */
EngSolo.RIGHT_CHAR_FLAG = 'sgm-right-char-flag';


/**
 * Напечатан правильный символ
 * @const {string}
 */
EngSolo.WRONG_CHAR_FLAG = 'sgm-wrong-char-flag';


/**
 *
 * @const {string}
 */
EngSolo.MISTAKE_FLAG = 'sgm-mistake-flag';


/**
 * Класс для выборки char объектов
 * @const {string}
 */
EngSolo.CHAR_JS = '.sgm-char__js';


/**
 * Класс для выборки line объектов
 * @const {string}
 */
EngSolo.LINE_JS = '.sgm-line__js';


/**
 * Таймаут на ошибку
 * @const {number}
 */
EngSolo.TIMEDOWN_FOR_ERROR = 5;


/**
 *
 * @const {string}
 */
EngSolo.EVENT_NAME = 'EngSolo';


/**
 *
 * @const {string}
 */
EngSolo.EVENT_MAKE_MISTAKE = 'EngSolo.MakeMistake';


/**
 *
 * @const {string}
 */
EngSolo.EVENT_TOO_MATCH_ERROR = 'EngSolo.ToMatchError';


/**
 *
 * @const {string}
 */
EngSolo.EVENT_END_GAME = 'EngSolo.EndGame';


/**
 * @const {string}
 */
EngSolo.EVENT_KEY_PRESS = 'EngSolo.KeyPress';


/**
 * @const {string}
 */
EngSolo.EVENT_KEY_PRESS_RIGHT = 'EngSolo.KeyPress.Right';


/**
 * @const {string}
 */
EngSolo.EVENT_ERROR_TIMEOUT = 'EngSolo.ErrorTimeout';


/**
 *
 * @type {Object}
 */
EngSolo.mapList = {};
