'use strict';
/* global EngSolo */



/**
 *
 * @param {number} count
 * @constructor
 */
EngSolo.KeyPressEvent = function(count) {
    this.count_ = count;
};


/**
 *
 * @return {number}
 */
EngSolo.KeyPressEvent.prototype.getCount = function() {
    return this.count_;
};
