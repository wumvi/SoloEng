'use strict';
/* global EngSolo */



/**
 *
 * @constructor
 */
EngSolo.RunController = function() {
};


/**
 *
 * @param {Array.<number>} mapList
 * @return {Object}
 */
EngSolo.RunController.prototype.getControllerList = function(mapList) {
  var list = {};
  for (var num = 0; num < mapList.length; num += 1) {
    var objId = /** @type {number} */ (mapList[num]);
    var map = new EngSolo.mapList[objId]();
    var $obj = jQuery('#eng-solo-' + objId);
    list[objId] = new EngSolo(map, $obj, 'solo-' + objId);
  }

  return list;
};
