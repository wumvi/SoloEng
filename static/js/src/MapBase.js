'use strict';
/* global EngSolo */



/**
 *
 * @constructor
 * @export
 */
EngSolo.MapBase = function() {

    /**
     *
     * @type {Array.<string>}
     * @private
     */
    this.lineList_ = [];

    /**
     *
     * @type {number}
     * @private
     */
    this.errorCount_ = EngSolo.MapBase.DEFAULT_KEY_ATTEMP_COUNT;

    /**
     *
     * @type {Object}
     * @private
     */
    this.charSoundList_ = {};

    /**
     *
     * @type {Object}
     * @private
     */
    this.charHideList_ = {};

    /**
     *
     * @type {Object}
     * @private
     */
    this.audioList_ = {};

    /**
     *
     * @type {number}
     * @private
     */
    this.globalOffset_ = 0;

    /**
     * @private
     * @type {HTMLBodyElement}
     */
    this.body_ = /** @type {HTMLBodyElement} */ (document.getElementsByTagName('body')[0]);

    /**
     * @type {number}
     * @private
     */
    this.gameDuration_ = EngSolo.MapBase.DEFAULT_GAME_DURATION;

    /**
     * @type {boolean}
     * @private
     */
    this.limitationFlag_ = false;

    /**
     *
     * @type {number}
     * @private
     */
    this.charCount_ = 0;

    /**
     * Количество добавленных озвученных фраз
     * @type {number}
     * @private
     */
    this.soundCountList_ = 0;
};


/**
 *
 * @const {number}
 */
EngSolo.MapBase.DEFAULT_KEY_ATTEMP_COUNT = 999;


/**
 *
 * @const {number}
 */
EngSolo.MapBase.DEFAULT_GAME_DURATION = 60 * 60;


/**
 *
 * @param {string} id
 * @param {string} soundUrl
 * @export
 */
EngSolo.MapBase.prototype.addSound = function(id, soundUrl) {
    this.soundCountList_ += 1;
    var audio = document.createElement('AUDIO');
    audio.src = soundUrl;
    audio.preload = this.soundCountList_ <= 1 ? 'auto' : 'none';
    this.audioList_[id] = audio;

    this.body_.appendChild(audio);
};


/**
 *
 * @param {number} num
 * @return {?EngSolo.CharSound}
 * @private
 */
EngSolo.MapBase.prototype.getNextSoundChar_ = function(num) {
    var flag = false;
    for (var key in this.charSoundList_) {
        if (!this.charSoundList_.hasOwnProperty(key)) {
            continue;
        }

        if (flag) {
            return this.charSoundList_[key];
        }

        if (parseInt(key, 10) === num) {
            flag = true;
        }
    }

    return null;
};


/**
 *
 * @param {string} id
 */
EngSolo.MapBase.prototype.loadAudio = function(id) {
    this.audioList_[id].load();
};


/**
 *
 * @param {string} id
 */
EngSolo.MapBase.prototype.playSound = function(id) {
    if (this.audioList_[id]) {
        this.audioList_[id].play();
    }
};


/**
 *
 * @return {number}
 */
EngSolo.MapBase.prototype.getCharCount = function() {
    return this.charCount_;
};


/**
 *
 */
EngSolo.MapBase.prototype.clearDom = function() {
    jQuery.each(this.audioList_, function(num, obj) {
        document.removeChild(obj);
    });
};


/**
 *
 * @param {string} textRaw
 * @export
 */
EngSolo.MapBase.prototype.addLine = function(textRaw) {
    var that = this;

    var textLength = textRaw.length;
    var localOffset = 0;
    var text = textRaw.replace(/\s+/, ' ');
    text = text.replace(/(?:\{[^}]+\})|\*|(?:#\w)/g, function(matchFull, index) {
        var key = that.globalOffset_ + index - localOffset;
        if (/#\w/.test(matchFull)) {
            textLength -= 2;
            localOffset += 2;
            that.charHideList_[key] = matchFull[1];
            return '';
        }

        if (matchFull === '*') {
            textLength -= 1;
            localOffset += 1;
            that.charHideList_[key] = matchFull[0];
            return '';
        }

        textLength -= matchFull.length;
        localOffset += matchFull.length;
        that.charSoundList_[key] = new EngSolo.CharSound(key, matchFull.substr(1, matchFull.length - 2));

        return '';
    });

    that.charCount_ += text.length;

    this.globalOffset_ += textLength;
    this.lineList_.push(text);
};


/**
 * Получаем количество ошиблок
 * @return {number} Количество ошибок
 */
EngSolo.MapBase.prototype.getErrorCount = function() {
    return this.errorCount_;
};


/**
 * Устанавливаем количество ошиблок
 * @param {number} errorCount
 * @export
 */
EngSolo.MapBase.prototype.setErrorCount = function(errorCount) {
    this.errorCount_ = errorCount;
};


/**
 * @return {Array.<string>}
 */
EngSolo.MapBase.prototype.getLineList = function() {
    return this.lineList_;
};


/**
 * @param {number} key
 * @return {?EngSolo.CharSound}
 */
EngSolo.MapBase.prototype.getCharSound = function(key) {
    return this.charSoundList_[key] ? this.charSoundList_[key] : null;
};


/**
 * @param {number} key
 * @return {?string}
 */
EngSolo.MapBase.prototype.getCharHide = function(key) {
    return this.charHideList_[key] ? this.charHideList_[key] : null;
};


/**
 * Получаем продолжительность игры
 * @return {number} Продолжительность игры в секундах
 */
EngSolo.MapBase.prototype.getGameDuration = function() {
    return this.gameDuration_;
};


/**
 * Устанавливаем продолжительность игры
 * @param {number} timeDuration Продолжительность игры в секундах
 * @export
 */
EngSolo.MapBase.prototype.setGameDuration = function(timeDuration) {
    this.gameDuration_ = timeDuration;
};


/**
 * Выставляем флаг того, что теперь при достижении лимитов, игра закончится
 * @param {boolean} flag
 * @export
 */
EngSolo.MapBase.prototype.setLimitationFlag = function(flag) {
    this.limitationFlag_ = flag;
};


/**
 *
 * @return {boolean}
 */
EngSolo.MapBase.prototype.getLimitationFlag = function() {
    return this.limitationFlag_;
};
