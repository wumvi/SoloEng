'use strict';
/* global EngSolo */


EngSolo.mapList[1] = (function() {
  /**
   * @extends {EngSolo.MapBase}
   * @constructor
   */
  EngSolo.MapItem1 = function() {
    EngSolo.MapBase.call(this);

    this.addSound('hewas', '/public/res/debugMp3/hewas.mp3');
    this.addSound('sheis', '/public/res/debugMp3/sheis.mp3');
    this.addSound('iam', '/public/res/debugMp3/iam.mp3');

    this.addLine('{hewas}#He was {sheis}She is {iam}I am {hewas}He was {sheis}She is {iam}I am');
    this.addLine('{hewas}He was {sheis}She is {iam}I am {hewas}He was {sheis}She is {iam}I am');
    this.addLine('{hewas}He was {sheis}She is {iam}I am {hewas}He was {sheis}She is {iam}I am');
    this.addLine('{hewas}He was {sheis}She is {iam}I am {hewas}He was {sheis}She is {iam}I am');
    this.addLine('{hewas}He was {sheis}She is {iam}I am {hewas}He was {sheis}She is {iam}I am');
    this.addLine('{hewas}He was {sheis}She is {iam}I am {hewas}He was {sheis}She is {iam}I am');
    this.addLine('{hewas}He was {sheis}She is {iam}I am {hewas}He was {sheis}She is {iam}I am');

    this.setErrorCount(3);
  };

  EngSolo.MapItem1.prototype = Object.create(EngSolo.MapBase.prototype);

  /**
   *
   * @type {Function}
   */
  EngSolo.MapItem1.prototype.constructor = EngSolo.MapItem1;

  return EngSolo.MapItem1;
})();
