'use strict';
/* global EventDomDispatcher */
/**
 * @param {EngSolo.MapBase} map
 * @param {!jQuery} $root
 * @param {string} subCategory
 * @constructor
 */
function EngSolo(map, $root, subCategory) {
    /**
   *
   * @type {EngSolo.MapBase}
   * @private
   */
    this.map_ = map;
    /**
   *
   * @type {!jQuery}
   * @private
   */
    this.$root_ = $root;
    /**
   *
   * @type {number}
   * @private
   */
    this.lineNum_ = 0;
    /**
   *
   * @type {number}
   * @private
   */
    this.charNum_ = 0;
    /**
   *
   * @type {number}
   * @private
   */
    this.globalCharNum_ = 0;
    /**
   *
   * @type {jQuery}
   * @private
   */
    this.$lineList_ = null;
    /**
   *
   * @type {jQuery}
   * @private
   */
    this.$charList_ = null;
    /**
   *
   * @type {number}
   * @private
   */
    this.errorCount_ = this.map_.getErrorCount();
    /**
   *
   * @type {?jQuery}
   * @private
   */
    this.$errorText_ = null;
    /**
   *
   * @type {boolean}
   * @private
   */
    this.canKeyPress_ = true;
    /**
   *
   * @type {?number}
   * @private
   */
    this.errorIntervalHandle_ = null;
    /**
   *
   * @type {number}
   * @private
   */
    this.mistakeCurrentTime_ = 0;
    /**
   * Активное ли задание
   * @type {boolean}
   * @private
   */
    this.isActive_ = true;
    /**
   *
   * @type {EventDomDispatcher}
   * @private
   */
    this.eventDomDispatcher_ = new EventDomDispatcher(EngSolo.EVENT_NAME, subCategory);
    this.init_();
}
/**
 *
 * @private
 */
EngSolo.prototype.init_ = function () {
    this.initMap_();
    this.$lineList_ = this.$root_.find(EngSolo.LINE_JS);
    this.clearAll_();
    this.initEvent_();
};
/**
 *
 * @private
 */
EngSolo.prototype.initMap_ = function () {
    var htmlText = '';
    var globalPos = 0;
    var list = this.map_.getLineList();
    for (var lineNum = 0; lineNum < list.length; lineNum += 1) {
        htmlText += '<div class="line line-js"><div class="inner">';
        for (var charNum = 0; charNum < list[lineNum].length; charNum += 1) {
            var char = this.map_.getCharHide(globalPos) ? '*' : list[lineNum][charNum];
            var className = char === ' ' ? 'space-flag' : '';
            htmlText += '<span class="char char-js ' + className + '">' + char + '</span>';
            globalPos += 1;
        }
        htmlText += '</div></div>';
    }
    htmlText += '<div class="error-msg">' + '<div class="text-box">\u0412\u044B \u0441\u043C\u043E\u0436\u0435\u0442\u0435 \u043F\u0440\u043E\u0434\u043E\u043B\u0436\u0438\u0442\u044C \u0447\u0435\u0440\u0435\u0437 <span class="text text-error-js">00</span></div>' + '</div><div class="sticker-box"></div>';
    this.$root_.html(htmlText);
    this.$errorText_ = this.$root_.find('.text-error-js');
};
/**
 *
 * @private
 */
EngSolo.prototype.initEvent_ = function () {
    var that = this;
    jQuery(document).bind('keydown', function (event) {
        that.onKeyDownAction_(event);
    });
    this.$root_.click(function () {
        if (!that.isActive_) {
            that.$root_.removeClass('not-active-flag');
            that.isActive_ = true;
        }
    });
};
/**
 *
 * @param {HTMLElement} elem
 * @return {boolean}
 * @private
 */
EngSolo.prototype.isInputTyping_ = function (elem) {
    return elem.tagName.toUpperCase() === 'INPUT' && (elem.type.toUpperCase() === 'TEXT' || elem.type.toUpperCase() === 'PASSWORD' || elem.type.toUpperCase() === 'FILE' || elem.type.toUpperCase() === 'SEARCH' || elem.type.toUpperCase() === 'EMAIL' || elem.type.toUpperCase() === 'NUMBER' || elem.type.toUpperCase() === 'DATE') || elem.tagName.toUpperCase() === 'TEXTAREA';
};
/**
 *
 * @param {jQuery.Event} event
 * @private
 */
EngSolo.prototype.onKeyDownAction_ = function (event) {
    var doPrevent = false;
    var elem = event.srcElement || event.target;
    // Блокировка backspace
    if (event.keyCode === 8) {
        if (this.isInputTyping_(elem)) {
            doPrevent = elem.readOnly || elem.disabled;
        } else {
            doPrevent = true;
        }
    } else {
        // Если выбран не документ, то мы набираем текст для задания
        // в ином случае, надо сделать не активным задание
        if (this.isInputTyping_(elem) && !elem.readOnly && !elem.disabled) {
            this.$root_.addClass('not-active-flag');
            this.isActive_ = false;
            return;
        }
        if (!this.canKeyPress_) {
            return;
        }
        var charCode = -1;
        var codeKey = event.originalEvent.code;
        // Когда сидим под IE, надо сделать обработку ctrl, shift, alft
        var ieChar = event.originalEvent.char;
        if (ieChar !== undefined && ieChar.length !== 1) {
            return;
        }
        // Если мы сидим под IE, то codeKey не передаётся как обычному Chrome
        if (codeKey === undefined) {
            charCode = event.originalEvent.keyCode;
        } else if (/^Key[A-Z]$/.test(codeKey)) {
            charCode = codeKey.substr(3).charCodeAt(0);
        } else if (codeKey === 'Space') {
            charCode = 32;
            event.preventDefault();
        } else if (codeKey === 'NumpadEnter' || codeKey === 'Enter') {
            return;
        }
        if (charCode !== -1) {
            this.keyPress_(charCode);
        }
    }
    if (doPrevent) {
        event.preventDefault();
    }
};
/**
 *
 * @param {number} charCode
 * @private
 */
EngSolo.prototype.keyPress_ = function (charCode) {
    // Игнор на клавишу Enter
    if (charCode === 13) {
        return;
    }

    this.eventDomDispatcher_.emit(EngSolo.EVENT_KEY_PRESS);

    // Получаем список линий
    var list = this.map_.getLineList();
    // Если игра пройдена, игнорим нажания
    if (this.lineNum_ >= list.length) {
        return;
    }
    // Получаем текущий символ, который нужно напечатать
    var charInStr = list[this.lineNum_][this.charNum_].toLowerCase();
    //.charCodeAt();
    // Проверяем, правильно ли мы нажали
    if (String.fromCharCode(charCode).toLowerCase() === charInStr) {
        this.makeRightKeyAction_();
    } else {
        this.makeWrongKeyAction_();
    }
};
/**
 *
 * @private
 */
EngSolo.prototype.makeWrongKeyAction_ = function () {
    var that = this;
    // Символ нажат не верно
    jQuery(this.$charList_[this.charNum_]).addClass(EngSolo.WRONG_CHAR_FLAG);
    this.$errorText_.text(EngSolo.TIMEDOWN_FOR_ERROR);
    this.canKeyPress_ = false;
    this.mistakeCurrentTime_ = EngSolo.TIMEDOWN_FOR_ERROR;
    this.errorIntervalHandle_ = setInterval(function () {
        that.intervalCountDownAction_();
    }, 1000);
    // Уменьшем количество допустимых ошибок
    this.errorCount_ -= 1;
    this.$root_.addClass(EngSolo.MISTAKE_FLAG);
    // Если количество допустимых ошибок, слишком много, то очищаем всё
    if (this.errorCount_ === 0) {
        this.$root_.addClass(EngSolo.TO_MATCH_MISTAKE_FLAG);
        //this.eventDomDispatcher_.emit(
        //  EngSolo.EVENT_TO_MATCH_ERROR,
        //  new EngSolo.ErrorMistakeModel(this.map_.getErrorCount(), true)
        //);
        this.clearAll_();
        this.eventDomDispatcher_.emit(EngSolo.EVENT_MAKE_MISTAKE, new EngSolo.ErrorMistakeModel(this.errorCount_, true));
    } else {
        this.eventDomDispatcher_.emit(EngSolo.EVENT_MAKE_MISTAKE, new EngSolo.ErrorMistakeModel(this.errorCount_, false));
    }
};
/**
 *
 * @private
 */
EngSolo.prototype.intervalCountDownAction_ = function () {
    this.mistakeCurrentTime_ -= 1;
    this.$errorText_.text(this.mistakeCurrentTime_);
    if (this.mistakeCurrentTime_ === 0) {
        clearInterval(this.errorIntervalHandle_);
        this.errorIntervalHandle_ = null;
        this.$root_.removeClass(EngSolo.MISTAKE_FLAG);
        this.canKeyPress_ = true;
    }
};
/**
 * @private
 */
EngSolo.prototype.makeRightKeyAction_ = function () {
    // Добавляем класс, что нажали мы правильно
    jQuery(this.$charList_[this.charNum_]).removeClass([EngSolo.WRONG_CHAR_FLAG, EngSolo.CURRENT_CHAR_FLAG].join(' ')).addClass(EngSolo.RIGHT_CHAR_FLAG);
    this.charNum_ += 1;
    this.globalCharNum_ += 1;
    // Получаем модель слова, которое должно произнести, если оно есть
    var charSound = this.map_.getCharSound(this.globalCharNum_);
    if (charSound) {
        // Воспроизводим слово
        this.playSound_(charSound);
    }
    // Получаем список линий
    var list = this.map_.getLineList();
    // Если это конец линии
    if (list[this.lineNum_].length === this.charNum_) {
        this.$lineList_.eq(this.lineNum_).addClass(EngSolo.LINE_DONE_FLAG);
        // Обнуляем позицию
        this.charNum_ = 0;
        // Переходим на след. линию
        this.lineNum_ += 1;
        // Если линии закончились, выходим. Пользователь удачно всё прошёл
        if (this.lineNum_ === list.length) {
            this.eventDomDispatcher_.emit(EngSolo.EVENT_END_GAME, null);
            return;
        }
        this.$charList_ = this.$lineList_.eq(this.lineNum_).find(EngSolo.CHAR_JS);
    }
    // Помечаем следующий символ, текущим, который нужно напечатать
    jQuery(this.$charList_[this.charNum_]).addClass(EngSolo.CURRENT_CHAR_FLAG);
};
/**
 *
 * @param {EngSolo.CharSound} charSound
 * @private
 */
EngSolo.prototype.playSound_ = function (charSound) {
    this.map_.playSound(charSound.getId());
};
/**
 *
 * @private
 */
EngSolo.prototype.clearAll_ = function () {
    // Выставляем текущую линию первой
    this.lineNum_ = 0;
    // Выставляем текущий символ первым
    this.charNum_ = 0;
    // Выставляем текущий глобальный символ первым
    this.globalCharNum_ = 0;
    // Очищаем количество ошибок
    this.errorCount_ = this.map_.getErrorCount();
    // Выставляем текущей первую линию
    this.$charList_ = this.$lineList_.eq(0).find(EngSolo.CHAR_JS);
    // Убераем флаги: правильный символ, текщий символ, неправильный символ
    this.$root_.find(EngSolo.CHAR_JS).removeClass([EngSolo.RIGHT_CHAR_FLAG, EngSolo.CURRENT_CHAR_FLAG, EngSolo.WRONG_CHAR_FLAG].join(' '));
    // Выставляем текущий символ в ноль
    this.$charList_.eq(0).addClass(EngSolo.CURRENT_CHAR_FLAG);
    // Флаг Линия сделана
    this.$lineList_.removeClass(EngSolo.LINE_DONE_FLAG);
};
/**
 * Устанавливаем, что данный объект активный и может перехватывать нажатия клавиш
 * @param {boolean} flag
 */
EngSolo.prototype.setActive = function (flag) {
    this.canKeyPress_ = flag;
};
/**
 * Линия пройдена
 * @const {string}
 */
EngSolo.LINE_DONE_FLAG = 'line-done-flag';
/**
 * Текущий символ
 * @const {string}
 */
EngSolo.CURRENT_CHAR_FLAG = 'current-char-flag';
/**
 * Напечатан правильный символ
 * @const {string}
 */
EngSolo.RIGHT_CHAR_FLAG = 'right-char-flag';
/**
 * Напечатан правильный символ
 * @const {string}
 */
EngSolo.WRONG_CHAR_FLAG = 'wrong-char-flag';
/**
 *
 * @const {string}
 */
EngSolo.MISTAKE_FLAG = 'mistake-flag';

/**
 *
 * @const {string}
 */
EngSolo.TO_MATCH_MISTAKE_FLAG = 'to-match-mistake-flag';

/**
 * Класс для выборки char объектов
 * @const {string}
 */
EngSolo.CHAR_JS = '.char-js';

/**
 * Класс для выборки line объектов
 * @const {string}
 */
EngSolo.LINE_JS = '.line-js';

/**
 *
 * @const {number}
 */
EngSolo.TIMEDOWN_FOR_ERROR = 5;

/**
 *
 * @const {string}
 */
EngSolo.EVENT_NAME = 'EngSolo';

/**
 *
 * @const {string}
 */
EngSolo.EVENT_MAKE_MISTAKE = 'EngSolo.MakeMistake';

/**
 *
 * @const {string}
 */
EngSolo.EVENT_TO_MATCH_ERROR = 'EngSolo.ToMatchError';

/**
 *
 * @const {string}
 */
EngSolo.EVENT_END_GAME = 'EngSolo.EndGame';

/**
 * @const {string}
 */
EngSolo.EVENT_KEY_PRESS = 'EngSolo.KeyPress';

/**
 *
 * @type {Object}
 */
EngSolo.mapList = {};
/* global EngSolo */
/**
 *
 * @param {number} num
 * @param {string} id
 * @constructor
 */
EngSolo.CharSound = function (num, id) {
    this.num_ = num;
    this.id_ = id;
};
/**
 *
 * @return {number}
 */
EngSolo.CharSound.prototype.getNum = function () {
    return this.num_;
};
/**
 *
 * @return {string}
 */
EngSolo.CharSound.prototype.getId = function () {
    return this.id_;
};
/* global EngSolo */
/**
 * @param {number} errorCount
 * @param {boolean} isToMatchError
 * @constructor
 */
EngSolo.ErrorMistakeModel = function (errorCount, isToMatchError) {
    /**
   *
   * @type {number}
   * @private
   */
    this.errorCount_ = errorCount;
    /**
   *
   * @type {boolean}
   * @private
   */
    this.isToMatchError_ = isToMatchError;
};
/**
 *
 * @return {number}
 */
EngSolo.ErrorMistakeModel.prototype.getErrorCount = function () {
    return this.errorCount_;
};
/**
 *
 * @return {boolean}
 */
EngSolo.ErrorMistakeModel.prototype.isToMatchError = function () {
    return this.isToMatchError_;
};
/* global EngSolo */
/**
 *
 * @constructor
 */
EngSolo.MapBase = function () {
    /**
   *
   * @type {Array.<string>}
   * @private
   */
    this.lineList_ = [];
    /**
   *
   * @type {number}
   * @private
   */
    this.errorCount_ = 3;
    /**
   *
   * @type {Object}
   * @private
   */
    this.charSoundList_ = {};
    /**
   *
   * @type {Object}
   * @private
   */
    this.charHideList_ = {};
    /**
   *
   * @type {Object}
   * @private
   */
    this.audioList_ = {};
    /**
   *
   * @type {number}
   * @private
   */
    this.globalOffset_ = 0;
    /**
   * @private
   * @type {HTMLBodyElement}
   */
    this.body_ = document.getElementsByTagName('body')[0];
};
/**
 *
 * @param {string} id
 * @param {string} soundUrl
 * @export
 */
EngSolo.MapBase.prototype.addSound = function (id, soundUrl) {
    var audio = document.createElement('AUDIO');
    audio.src = soundUrl;
    // audio.preload = 'yes';
    this.audioList_[id] = audio;
    this.body_.appendChild(audio);
};
/**
 *
 * @param {string} id
 */
EngSolo.MapBase.prototype.playSound = function (id) {
    if (this.audioList_[id]) {
        this.audioList_[id].play();
    }
};
/**
 *
 */
EngSolo.MapBase.prototype.clearDom = function () {
    jQuery.each(this.audioList_, function (num, obj) {
        document.removeChild(obj);
    });
};
/**
 *
 * @param {string} textRaw
 * @export
 */
EngSolo.MapBase.prototype.addLine = function (textRaw) {
    var that = this;
    var textLength = textRaw.length;
    var localOffset = 0;
    var text = textRaw.replace(/\s+/, ' ');
    text = text.replace(/(\{[^}]+\})|#/g, function (matchFull, match, index) {
        var key = that.globalOffset_ + index - localOffset;
        if (matchFull === '#') {
            textLength -= 1;
            localOffset += 1;
            that.charHideList_[key] = text[index + 1];
            return '';
        }
        textLength -= matchFull.length;
        localOffset += matchFull.length;
        that.charSoundList_[key] = new EngSolo.CharSound(key, matchFull.substr(1, matchFull.length - 2));
        return '';
    });
    this.globalOffset_ += textLength;
    this.lineList_.push(text);
};
/**
 * Получаем количество ошиблок
 * @return {number} Количество ошибок
 */
EngSolo.MapBase.prototype.getErrorCount = function () {
    return this.errorCount_;
};
/**
 * Устанавливаем количество ошиблок
 * @param {number} errorCount
 * @export
 */
EngSolo.MapBase.prototype.setErrorCount = function (errorCount) {
    this.errorCount_ = errorCount;
};
/**
 * @return {Array.<string>}
 */
EngSolo.MapBase.prototype.getLineList = function () {
    return this.lineList_;
};
/**
 * @param {number} key
 * @return {?EngSolo.CharSound}
 */
EngSolo.MapBase.prototype.getCharSound = function (key) {
    return this.charSoundList_[key] ? this.charSoundList_[key] : null;
};
/**
 * @param {number} key
 * @return {?string}
 */
EngSolo.MapBase.prototype.getCharHide = function (key) {
    return this.charHideList_[key] ? this.charHideList_[key] : null;
};
/* global EngSolo */
/**
 *
 * @constructor
 */
EngSolo.RunController = function () {
};
/**
 *
 * @param {Array.<number>} mapList
 * @return {Object}
 */
EngSolo.RunController.prototype.getControllerList = function (mapList) {
    var list = {};
    for (var num = 0; num < mapList.length; num += 1) {
        var objId = mapList[num];
        var map = new EngSolo.mapList[objId]();
        var $obj = jQuery('#eng-solo-' + objId);
        list[objId] = new EngSolo(map, $obj, 'solo-' + objId);
    }
    return list;
};