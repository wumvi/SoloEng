'use strict';
/* global require */

var eol = require('gulp-eol');
var gulp = require('gulp');
var merge = require('merge-stream');

var cssBinDir = './public/res/css/bin/';
var cssMinDir = './public/res/css/min/';
var cssSrcDir = './static/sass/';

// Очистка директорий CSS, от возможного муссора
var rm = require('gulp-rimraf');
gulp.task('clean-css', function() {
  return gulp.src([cssBinDir + '*', cssMinDir + '*']).pipe(rm());
});

// Перевод SASS в CSS
var sass = require('gulp-sass');
gulp.task('sass', ['clean-css'], function() {
  return gulp.src(cssSrcDir + '*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(eol())
    .pipe(gulp.dest(cssBinDir));
});

// Минификация CSS
var cssnano = require('gulp-cssnano');
gulp.task('minify-css', ['sass'], function() {
  return gulp.src(cssBinDir + '*.css')
    .pipe(cssnano())
    .pipe(gulp.dest(cssMinDir));
});

var jsBinDir = './public/res/js/bin/';
var jsMinDir = './public/res/js/min/';
var jsConcatDir = './public/res/js/concat/';
var jsSrc = './static/js/';


var concat = require('gulp-concat-util');
var yaml = require('js-yaml');
var fs = require('fs');
var removeUseStrict = require('gulp-remove-use-strict');

// Очистка директорий CSS, от возможного муссора
gulp.task('clean-js', function() {
  return gulp.src([jsBinDir + '*', jsMinDir + '*']).pipe(rm());
});

gulp.task('concat-js', function() {
  var buildProperties = yaml.safeLoad(fs.readFileSync('static/js/build.yaml', 'utf8'));
  return gulp.src(buildProperties.list)

      .pipe(concat(buildProperties.name))
      .pipe(removeUseStrict())
      .pipe(gulp.dest(jsConcatDir));
});
